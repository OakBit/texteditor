﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using XAMLEditor.Model;

namespace XAMLEditor.ViewModel
{
    class MainWindowViewModel : DependencyObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        ObservableCollection<TextFile> files;

        public MainWindowViewModel()
        {
            files = new ObservableCollection<TextFile>();
        }
        

        public void NewDocument()
        {
            files.Add(new TextFile());
        }
        public bool CanSaveDocument(int fileIndex)
        {
            return fileIndex >= 0 && fileIndex < files.Count;
        }

        public void ApplyToSelection(DependencyProperty property, object value)
        {
            //if (value != null)
            //    _textBox.Selection.ApplyPropertyValue(property, value);
        }

        public bool OpenDocument()
        {
            //OpenFileDialog dlg = new OpenFileDialog();

            //if (dlg.ShowDialog() == true)
            //{
            //    _currentFile = dlg.FileName;

            //    using (Stream stream = dlg.OpenFile())
            //    {
            //        TextRange range = new TextRange(_textBox.Document.ContentStart, _textBox.Document.ContentEnd);
            //        range.Load(stream, DataFormats.Rtf);
            //    }

            //    return true;
            //}

            return false;
        }

        public bool SaveDocument(int fileIndex, RichTextBox file)
        {
            //if (string.IsNullOrEmpty(_currentFile)) return SaveDocumentAs();
            //else
            //{
            //    using (Stream stream = new FileStream(_currentFile, FileMode.Create))
            //    {
            //        TextRange range = new TextRange(_textBox.Document.ContentStart, _textBox.Document.ContentEnd);
            //        range.Save(stream, DataFormats.Rtf);
            //    }

            //    return true;
            //}
            return true;
        }

        public bool SaveDocumentAs()
        {
            //SaveFileDialog dlg = new SaveFileDialog();

            //if (dlg.ShowDialog() == true)
            //{
            //    _currentFile = dlg.FileName;
            //    return SaveDocument();
            //}

            return false;
        }
    }
}
