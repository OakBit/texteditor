﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using XAMLEditor.ViewModel;

namespace XAMLEditor.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowViewModel viewModel = new MainWindowViewModel();


        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = viewModel;
        }
        private TabItem CreateTab(string name)
        {
            TabItem ti = new TabItem();
            ti.Header = name;
            RichTextBox tb = new RichTextBox();
            tb.SelectionChanged += body_SelectionChanged;
            tb.SpellCheck.IsEnabled = true;
            tb.AcceptsReturn = true;
            tb.AcceptsTab = true;
            tb.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;

            ti.Content = tb;

            //ti.CommandBindings.Add(new CommandBinding(CloseTabCommand, TabClose));

            return ti;
        }

        public void RemoveTab(string name)
        {
            for (int i = 0; i < docTabs.Items.Count; i++)
            {
                TabItem item = (TabItem)docTabs.Items[i];

                if (item.Header.Equals(name))
                {
                    docTabs.Items.Remove(item);
                    break;
                }
            }
        }
        private void CanCloseTab(object sender, CanExecuteRoutedEventArgs e)
        {
            if (docTabs.Items.Count > 0)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        private void CloseTab(object sender, ExecutedRoutedEventArgs e)
        {
            docTabs.Items.Remove(docTabs.SelectedItem);
        }


        private void CanComment(object sender, CanExecuteRoutedEventArgs e)
        {

        }

        private void CanUncomment(object sender, CanExecuteRoutedEventArgs e)
        {

        }

        private void NewDocument(object sender, ExecutedRoutedEventArgs e)
        {
            TabItem item = CreateTab(string.Format("new file {0}", docTabs.Items.Count + 1));
            docTabs.Items.Add(item);

            viewModel.NewDocument();

            item.Focus();
        }

        private void OpenDocument(object sender, ExecutedRoutedEventArgs e)
        {
            viewModel.OpenDocument();
        }

        private void SaveDocument(object sender, ExecutedRoutedEventArgs e)
        {
            if (viewModel.CanSaveDocument(docTabs.SelectedIndex))
                viewModel.SaveDocument(docTabs.SelectedIndex, (RichTextBox)docTabs.SelectedContent);
        }

        private void SaveDocumentAs(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void PrintPreview(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void PrintDocument(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void TabClose(object sender, ExecutedRoutedEventArgs e)
        {
            
        }

        private void ApplicationClose(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void TextEditorToolbar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void body_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void SaveDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (docTabs.Items.Count > 0)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }


    }
}
