﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XAMLEditor.View
{
    /// <summary>
    /// Interaction logic for XAMLEditorToolbar.xaml
    /// </summary>
    public partial class XAMLEditorToolbar : UserControl
    {
        public static readonly RoutedUICommand LineNumberCommand = new RoutedUICommand(
                        "Line Number",
                        "LineNumberCommand",
                        typeof(XAMLEditorToolbar),
                        new InputGestureCollection() { }
                );
        public static readonly RoutedUICommand UncommentCommand = new RoutedUICommand(
                        "Uncomment",
                        "UncommentCommand",
                        typeof(XAMLEditorToolbar),
                        new InputGestureCollection() { }
                );
        public static readonly RoutedUICommand CommentCommand = new RoutedUICommand(
                                "Comment",
                                "CommentCommand",
                                typeof(XAMLEditorToolbar),
                                new InputGestureCollection() { }
                        );

        public XAMLEditorToolbar()
        {
            InitializeComponent();
        }
        public bool IsSynchronizing { get; private set; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public void SynchronizeWith(TextSelection selection)
        {
            IsSynchronizing = true;

            Synchronize<TextDecorationCollection>(selection, TextBlock.TextDecorationsProperty, SetTextDecoration);

            IsSynchronizing = false;
        }

        private static void Synchronize<T>(TextSelection selection, DependencyProperty property, Action<T> methodToCall)
        {
            object value = selection.GetPropertyValue(property);
            if (value != DependencyProperty.UnsetValue) methodToCall((T)value);
        }

       

        private void SetTextDecoration(TextDecorationCollection decoration)
        {
        }
        
    }
}
