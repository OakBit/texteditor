﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XAMLEditor.View
{
    /// <summary>
    /// Interaction logic for XAMLEditorMenu.xaml
    /// </summary>
    public partial class XAMLEditorMenu : UserControl
    {
        public static readonly RoutedUICommand CloseTabCommand = new RoutedUICommand(
                                "Close Tab",
                                "CloseTabCommand",
                                typeof(XAMLEditorMenu),
                                new InputGestureCollection(){}
                        );

        public XAMLEditorMenu()
        {
            InitializeComponent();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This text editor was created by Florian Jedelhauser.", "About");
        }

    }
}
